from django.urls import path
from . import views

urlpatterns = [
    #vehicles/
    path('', views.index_view, name='vehicle_index'),
    #vehicles/id
    path('<int:nid>', views.detail_view, name='vehicles_detail'),
    #vehicles/new
    path('new', views.create_view, name='vehicle_new'),
    #vehicles/edit/id
    path('edit/<int:nid>', views.update_view, name='vehicle_update'),
    #vehicles/delete/id
    path('delete/<int:nid>', views.delete_view, name='vehicle_delete'),
]
