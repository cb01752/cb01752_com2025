from django import forms
from .models import Vehicle

class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type: 'time'
# creating a form
class VehicleForm(forms.ModelForm):
    
    # create meta class
    class Meta:
        # specify model to be used
        model = Vehicle
        
        fields = ['regNumber', 'owner', 'contactNo', 'parkingDate', 'timeIn', 'timeOut']
        widgets = {
            'regNumber': forms.TextInput(attrs={
            'class': 'formfield',
            'placeholder': 'Registration Number',
            }),
            'owner': forms.TextInput(attrs={
            'class': 'formfield',
            'placeholder': 'Full Name',
            }),
            'contactNo': forms.TextInput(attrs={
            'class': 'formfield',
            'placeholder': 'Phone Number',
            }),
            
            'parkingDate': DateInput(),
            
            'timeIn': TimeInput(attrs={'placeholder' : 'hh:mm'}),
            
            'timeOut': TimeInput(),
        }