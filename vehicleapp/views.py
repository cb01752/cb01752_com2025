from django.http import HttpResponse
from django.shortcuts import (get_object_or_404, render, redirect)
from django.contrib import messages
from .models import Vehicle
from .forms import VehicleForm

def index_view(request):
    # dictionary for initial data with
    # field names as keys
    context ={}
    
    # add the dictionary during initialization
    context["vehicle_list"] = Vehicle.objects.all()
    
    return render(request, "vehicleapp/index.html", context)

# pass id attribute from urls
def detail_view(request, nid):
    context ={}

    # add the dictionary during initialization
    context["vehicle"] = get_object_or_404(Vehicle, pk=nid)
    return render(request, "vehicleapp/detail_view.html",context)

def create_view(request):
    context ={}
    form = VehicleForm(request.POST or None)
    if(request.method == 'POST'):
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Vehicle Space Created')
            return redirect('vehicle_index')
        else:
            messages.add_message(request, messages.ERROR, 'Invalid Form Data; Space not created')
            
    context['form']= form
    return render(request, "vehicleapp/create_view.html", context)

def update_view(request, nid):
    context ={}
    
    # fetch the object related to passed id
    obj = get_object_or_404(Vehicle, id = nid)
    
    # pass the object as instance in form
    form = VehicleForm(request.POST or None, instance = obj)

    # save the data from the form and
    # redirect to detail_view
    if form.is_valid():
        form.save()
        messages.add_message(request, messages.SUCCESS, 'VehicleUpdated')

        return redirect('vehicle_detail', nid=nid)
    # add form dictionary to context
    context["form"] = form
    return render(request, "vehicleapp/update_view.html", context)

def delete_view(request, nid):
    # fetch the object related to passed id
    obj = get_object_or_404(Vehicle, id = nid)
    # delete object
    obj.delete()
    messages.add_message(request, messages.SUCCESS, 'Vehicle Deleted')
    # after deleting redirect to index view
    return redirect('vehicle_index')