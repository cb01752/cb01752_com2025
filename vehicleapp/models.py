from django.db import models

class Vehicle(models.Model):
    regNumber = models.CharField(max_length = 7, unique=True)
    owner = models.CharField(max_length = 50, unique=True)
    contactNo = models.CharField(max_length = 11, unique=True)
    parkingDate = models.DateField()
    timeIn = models.TimeField()
    timeOut = models.TimeField()