# Generated by Django 4.1.1 on 2022-12-06 05:34

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Vehicle",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("regNumber", models.CharField(max_length=7, unique=True)),
                ("owner", models.CharField(max_length=50, unique=True)),
                ("contactNo", models.CharField(max_length=11, unique=True)),
                ("parkingDate", models.DateField()),
                ("timeIn", models.CharField(max_length=50)),
                ("timeOut", models.CharField(max_length=50)),
            ],
        ),
    ]
