from django.test import TestCase
from django.urls import reverse 

class HomePageTests(TestCase):
    """Test the carvehicle application homepage"""

    def setUp(self):
        return

    def test_homepage(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)
        
        self.assertContains(response, 'This is My Home Page')
        self.assertContains(response, 'This is my footer')
        
    def test_contact(self):
        response = self.client.get(reverse('contact'))
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, 'Contact')